import {useCallback, useState} from "react"

// Toggle
const useToggle = ((initialState: boolean): [state: boolean, toggle: () => void] => {
    const [state, setState] = useState(initialState);

    const toggle = useCallback(() => {
        setState(currState => !currState);
    }, [])

    return [state, toggle];

});

export default useToggle;
